# Welcome to K2Games

[K2Games - Learning by playing](https://k2games.info/) is a strategic partnership project for an innovative approach to education for sustainability, providing youth workers with board games, simulation games, and guidance to develop their own games or use existing games as educational tools.

The K2Games project was co-founded by the Erasmus+ Programme of the European Union.

## Follow Us
Stay updated on the latest news, courses, and resources by following us on social media:
- [Facebook](https://www.facebook.com/groups/k2gamescluj)
