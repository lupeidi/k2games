import React from "react";
import { FormattedMessage } from "react-intl";

export default class Contact extends React.Component {
  render() {
    return (
      <section className="text-dark w-full body-font pt-24 sm:pt-40 xl:pt-44">

        <div className="container px-5 pb-12 mx-auto">
          <div className="lg:w-3/4 w-full mx-auto text-left">

            <h1 className="pb-12 leading-normal font-bold text-6xl">
              <FormattedMessage id='about.title' defaultMessage='default'/>
            </h1> 
            <p className="leading-normal tracking-wide text-lg pb-8">
              <FormattedMessage id='about.description' defaultMessage='default'/>
            </p>

            {/* The who */}
            <h2 className="py-12 leading-normal font-bold text-2xl">
              <FormattedMessage id='about.subtitle3' defaultMessage='default'/>
            </h2> 
            <p className="leading-normal tracking-wide text-lg pb-8">
              <FormattedMessage id='about.6' defaultMessage='default'/>
            </p>

          </div>
        </div>

        {/* ngo info */}
        <div className="min-w-screen bg-orange flex items-center p-5 lg:p-10 overflow-hidden relative">
          <div className="w-2/3 max-w-12xl rounded-lg bg-background shadow-xl p-5 lg:p-20 mx-auto text-dark relative flex lg:flex-row flex-col">
            <div className='m-auto md:pb-8 pb-0'>
              <img src='/partnerLogos.png'  className='m-auto'/>
            </div>
            <div className='m-auto md:text-left text-center'>
              <p className="leading-normal tracking-wide text-lg pb-8 px-6 md:px-12 pt-6 md:pt-0">
                <a target="_blank" href='http://responsibleconsumption.info/' className='hover:shadow-linkThick shadow-link '>
                  <FormattedMessage id='about.src' defaultMessage='default'/>
                </a>
                <FormattedMessage id='about.srcdescription' defaultMessage='default'/>

              </p>    
              <p className="leading-normal tracking-wide text-lg pb-8 px-6 md:px-12 pt-6 md:pt-0">
                <a target="_blank" href='https://www.facebook.com/INSEPD/' className='hover:shadow-linkThick shadow-link '>
                  <FormattedMessage id='about.insight' defaultMessage='default'/>
                </a>
                <FormattedMessage id='about.insightdescription' defaultMessage='default'/>
              </p>  
              <p className="leading-normal tracking-wide text-lg pb-8 px-6 md:px-12 pt-6 md:pt-0">
                <a target="_blank" href='http://www.eehyc.org/' className='hover:shadow-linkThick shadow-link '>
                  <FormattedMessage id='about.eehyc' defaultMessage='default'/>
                </a>
                <FormattedMessage id='about.eehycdescription' defaultMessage='default'/>

              </p>  
              <p className="leading-normal tracking-wide text-lg pb-8 px-6 md:px-12 pt-6 md:pt-0">
                <a target="_blank" href='https://crisp-berlin.org/' className='hover:shadow-linkThick shadow-link '>
                  <FormattedMessage id='about.crisp' defaultMessage='default'/>
                </a>
                <FormattedMessage id='about.crispdescription' defaultMessage='default'/>
              </p>  
              <p className="leading-normal tracking-wide font-bold text-lg pb-8 px-6 md:px-12 pt-6 md:pt-0">
                <a target="_blank" href='https://www.facebook.com/groups/k2gamescluj'>
                  <FormattedMessage id='closing.group' defaultMessage='default'/>&rarr; </a>
              </p>
            </div>
            
          </div>
        </div>

        <div className="container px-5 pb-12 mx-auto">
          <div className="lg:w-3/4 w-full mx-auto text-left">

            {/* The what */}
            <h2 className="py-12 leading-normal font-bold text-2xl">
              <FormattedMessage id='about.subtitle' defaultMessage='default'/>
            </h2> 
            <p className="leading-normal tracking-wide text-lg pb-8">
              <FormattedMessage id='about.1' defaultMessage='default'/>
            </p>
            <p className="leading-normal tracking-wide text-lg pb-8">
              <FormattedMessage id='about.2' defaultMessage='default'/>
            </p>  
            <p className="leading-normal tracking-wide text-lg pb-8">
              <FormattedMessage id='about.2b' defaultMessage='default'/>
              <a href='/guide' className='shadow-link hover:shadow-linkThick'> <FormattedMessage id='about.link' defaultMessage='default'/></a>
              <FormattedMessage id='about.2c' defaultMessage='default'/>

            </p> 


            {/* The why */}
            <h2 className="py-12 leading-normal font-bold text-2xl">
              <FormattedMessage id='about.subtitle2' defaultMessage='default'/>
            </h2> 
            <div className="max-w-md lg:max-w-2xl w-full mx-auto py-12">
              <img src='/aboutus.svg' alt="standing out" className='mx-auto'/>
            </div>
            <p className="leading-normal tracking-wide text-lg pb-8">
              <FormattedMessage id='about.3' defaultMessage='default'/>
            </p>
            <p className="leading-normal tracking-wide text-lg pb-8">
              <FormattedMessage id='about.4' defaultMessage='default'/>
            </p> 
            <p className="leading-normal tracking-wide text-lg pb-16">
              <FormattedMessage id='about.5' defaultMessage='default'/>
            </p> 

            <span className="inline-block h-1 w-full rounded bg-orange mt-8 mb-6"></span>
            <p className="leading-normal tracking-wide text-lg pb-8 text-center">
              <FormattedMessage id='about.contact' defaultMessage='default'/>
            </p> 
            <p className="leading-normal font-bold tracking-wide text-lg pb-8 text-center">
              <FormattedMessage id='about.contact2' defaultMessage='default'/>
            </p> 

            <div className='m-auto md:pb-8 pb-0'>
              <img src='/srcMail.png'  className='m-auto'/>
            </div>          
          </div>
        </div>

      </section>
    )
  }
}
