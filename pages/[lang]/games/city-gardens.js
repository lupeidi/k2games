import React from "react";
import GameIntro from './../../../src/components/gameIntro';
import GameTabs from './../../../src/components/gameTabs';

export default class RecyclingParty extends React.Component {
  render() {
    return (
      <>
        <div className="flex flex-wrap pt-32 sm:pt-40 xl:pt-44">
          <div className="w-full">

            {/* Content text */}
            <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded">
              <div className="px-4 py-5 flex-auto">
                <div className="tab-content tab-space">

                  <GameIntro 
                    title={'cityGardens.title'}
                    description={'cityGardens.description'}
                    descriptionB={'cityGardens.descriptionB'}
                    image={'/gameLogos/K2 City Gardens logo - white background.png'}
                    imageAlt={'City Gardens'}
                    buttonColor={'bg-city-gardens'}
                    downloadPath={'/gameResources/K2 City Gardens.zip'}
                    downloadAlt={'K2 City Gardens'}
                  />

                </div>
              </div>
            </div>
            {/* Tabs */}
            <GameTabs/>
          </div>
        </div>
      </>
    );
  }
};
