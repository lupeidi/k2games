import React from "react";
import GameIntro from './../../../src/components/gameIntro';
import GameTabs from './../../../src/components/gameTabs';

export default class RecyclingParty extends React.Component {
  render() {
    return (
      <>
        <div className="flex flex-wrap pt-32 sm:pt-40 xl:pt-44">
          <div className="w-full">

            {/* Content text */}
            <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded">
              <div className="px-4 py-5 flex-auto">
                <div className="tab-content tab-space">
                  <GameIntro
                    title={'recyclingParty.title'}
                    description={'recyclingParty.description'}
                    descriptionB={'recyclingParty.descriptionB'}
                    image={'/gameLogos/K2 Recycling party logo - white background.png'}
                    imageAlt={'Recycling Party'}
                    buttonColor={'bg-recycling-party'}
                    downloadPath={'/gameResources/K2 Recycling Party.zip'}
                    downloadAlt={'K2 Recycling Party'}
                  />
                </div>
              </div>
            </div>
            {/* Tabs */}
            <GameTabs/>
          </div>
        </div>
      </>
    );
  }
};
