import App from 'next/app';
import { IntlProvider } from 'react-intl';
import { configureLanguage } from './../utils/language';
import flatten from './../utils/flatten';
import Layout from './../src/layouts/mainLayout';
import React from "react";

import '../styles/globals.css'
import en from './../lang/en.json';
import ro from './../lang/ro.json';

export default class MyApp extends App {
  constructor(props) {
    super(props);
    this.state = {
      locale: 'ro'
    }
  }

  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};
    if(Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }
    // const language = configureLanguage(ctx);

    return { pageProps };
  }

  render(){
    const { Component, pageProps } = this.props;
    const language = 'en';
    const messages = {
      'ro': ro,
      'en': en,
    }

    return(
      <IntlProvider locale={language} messages={messages[language]}>
        <Layout content={
          <Component selectedLanguage={language} {...pageProps}/>
        } />
      </IntlProvider>
    )
  }
}
