import React from "react";
import GameIntro from './../../src/components/gameIntro';
import GameTabs from './../../src/components/gameTabs';

export default class RecyclingParty extends React.Component {
  render() {
    return (

      <>
        <div className="flex flex-wrap pt-32 sm:pt-40 xl:pt-44">
          <div className="w-full">

            {/* Content text */}
            <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded">
              <div className="px-4 py-5 flex-auto">
                <div className="tab-content tab-space">

                  <GameIntro 
                    title={'airQuality.title'}
                    description={'airQuality.description'}
                    descriptionB={'airQuality.descriptionB'}
                    image={'/gameLogos/K2 Air Quality logo - white background.png'}
                    imageAlt={'Air Quality'}
                    buttonColor={'bg-air-quality'}
                    downloadPath={'/gameResources/K2 Air Quality.zip'}
                    downloadAlt={'K2 Air Quality'}
                  />

                </div>
              </div>
            </div>
            {/* Tabs */}
            <GameTabs/>
          </div>
        </div>
      </>
    );
  }
};
