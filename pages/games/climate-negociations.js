import React from "react";
import { FormattedMessage } from "react-intl";
import GameIntro from './../../src/components/gameIntro';
import GameTabs from './../../src/components/gameTabs';

export default class RecyclingParty extends React.Component {
  render() {
    return (

      <>
        <div className="flex flex-wrap pt-32 sm:pt-40 xl:pt-44">
          <div className="w-full">

            {/* Content text */}
            <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded">
              <div className="px-4 py-5 flex-auto">
                <div className="tab-content tab-space">
                  <div className="w-full p-1 py-8 md:px-8">
                    <h2 className="leading-relaxed text-left text-5xl font-bold pb-6 px-6">
                      <FormattedMessage id='climateNegociations.title' defaultMessage='default'/>
                    </h2> 
                  </div>

                  <GameIntro 
                    title={'climateNegociations.title1'}
                    description={'climateNegociations.description1'}
                    descriptionB={'climateNegociations.description1B'}
                    image={'/gameLogos/K2 Climate Negociations logo - white background.png'}
                    imageAlt={'Climate Negociations Part One'}
                    buttonColor={'bg-climate-negociations'}
                    downloadPath={'/gameResources/K2 Climate Negotiations - Game.pdf'}
                    downloadAlt={'K2 Climate Negociations'}
                    preview={true}
                  />
                  <GameIntro 
                    title={'climateNegociations.title2'}
                    description={'climateNegociations.description2'}
                    descriptionB={'climateNegociations.description2B'}
                    image={'/gameLogos/K2 Climate Negociations logo - white background.png'}
                    imageAlt={'Climate Negociations Part Two'}
                    buttonColor={'bg-climate-negociations'}
                    downloadPath={'/gameResources/K2 Climate Negotiations - Game.pdf'}
                    downloadAlt={'K2 Climate Negociations'}
                    preview={true}
                  />

                </div>
              </div>
            </div>
            {/* Tabs */}
            <GameTabs/>
          </div>
        </div>
      </>
    );
  }
};
