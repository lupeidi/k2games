import React from "react";
import { FormattedMessage } from "react-intl";
import GameTabs from './../../src/components/gameTabs';

export default class Games extends React.Component {
  render() {
    return (
      <section className="text-dark w-full body-font pt-24 sm:pt-40 xl:pt-44">

        <div className="container px-5 pb-12 mx-auto">
          <div className="lg:w-3/4 w-full mx-auto text-left">

            <h1 className="pb-12 leading-normal font-bold text-6xl">
              <FormattedMessage id='games.title' defaultMessage='default'/>
            </h1> 
            <p className="leading-normal tracking-wide text-lg pb-8">
              <FormattedMessage id='about.1' defaultMessage='default'/>
            </p>
            <p className="leading-normal tracking-wide text-lg pb-8">
              <FormattedMessage id='games.1' defaultMessage='default'/>
            </p>
          </div>
        </div>
        <GameTabs/>
      </section>
    )
  }
}
