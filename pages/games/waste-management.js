import React from "react";
import GameIntro from './../../src/components/gameIntro';
import GameTabs from './../../src/components/gameTabs';

export default class RecyclingParty extends React.Component {
  render() {
    return (

      <>
        <div className="flex flex-wrap sm:pt-40 xl:pt-44">
          <div className="w-full">

            {/* Content text */}
            <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded">
              <div className="px-4 py-5 flex-auto">
                <div className="tab-content tab-space">

                  <GameIntro 
                    title={'wasteManagement.title'}
                    description={'wasteManagement.description'}
                    descriptionB={'wasteManagement.descriptionB'}
                    image={'/gameLogos/K2 Waste Management logo - white background.png'}
                    imageAlt={'Waste Management'}
                    buttonColor={'bg-waste-management'}
                    downloadPath={'/gameResources/K2 Waste Management - Game.pdf'}
                    downloadAlt={'K2 Waste Management'}
                    preview={true}
                  />

                </div>
              </div>
            </div>
            {/* Tabs */}
            <GameTabs/>
          </div>
        </div>
      </>
    );
  }
};
