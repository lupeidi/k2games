import React from "react";
import Intro from '../src/components/chapters/introOne';
import ChapterOne from '../src/components/chapters/chapterOne';
import ChapterTwo from '../src/components/chapters/chapterTwo';
import ChapterThree from '../src/components/chapters/chapterThree';
import ChapterFour from '../src/components/chapters/chapterFour';
import Closing from "../src/components/chapters/closing";
import ChapterX from "../src/components/chapters/chapterX";
import Preface from "../src/components/chapters/preface";

export default class Guide extends React.Component {
  render() {
    return (
      <div className="flex flex-col items-center justify-center py-2 sm:pt-16 xl:pt-20">
        <Intro/>
        <ChapterOne/>
        <ChapterTwo/>
        <ChapterThree/>
        <ChapterFour/>
        <ChapterX/>
        <Closing/>
      </div>
    )
  }
}
