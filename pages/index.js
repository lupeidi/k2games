import React from "react";
import CircleNav from '../src/components/circleNav';
import IntroHero from '../src/components/introHero';
import Preface from '../src/components/chapters/preface';

import Head from 'next/head';
import { FormattedMessage } from "react-intl";

export default class Home extends React.Component {
  render() {
    return (
      <div>
        <Head>
          <title>K2Games</title>
          <meta name="description" content="The “K2Games - Learning by playing” community helps you make education for sustainability, environment and health more engaging, meaningful and impactful." key="description" />
        </Head>
        <IntroHero className='sm:pt-16 xl:pt-20'/>
        <Preface/>
        <div className="hidden md:block">
          <CircleNav/>
        </div>

        <div className="block md:hidden p-5 bg-orange">
          <div className='rounded-lg my-6 py-12 bg-background shadow-lg'>
 
          <h2 className="pb-12 leading-normal font-bold text-center text-lg">
            <FormattedMessage id='landing.nav' defaultMessage='default'/>
          </h2> 

            {/* guide */}
            <p className="leading-normal text-center tracking-wide text-lg">
              <a href='/guide' className='shadow-link hover:shadow-linkThick'>
                <FormattedMessage id='link.1' defaultMessage='default'/>
              </a>
            </p>
            {/* games */}
            <p className="leading-normal tracking-wide text-center text-lg">
              <a href='/games' className='shadow-link hover:shadow-linkThick'>
                <FormattedMessage id='link.2' defaultMessage='default'/>
                </a>
            </p>
          {/* aboutus */}
            <p className="leading-normal tracking-wide text-center text-lg">
              <a href='/aboutus' className='shadow-link hover:shadow-linkThick'>
                <FormattedMessage id='link.4' defaultMessage='default'/>
              </a>
            </p>
            {/* group */}
            <p className="leading-normal text-center tracking-wide text-lg">
              <a target="_blank" href='https://www.facebook.com/groups/k2gamescluj' className='shadow-link hover:shadow-linkThick'>
                <FormattedMessage id='link.3' defaultMessage='default'/>
              </a>
            </p>
          </div>

        </div>
      </div>
    )
  }
}
