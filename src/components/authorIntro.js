import React from "react";
import { FormattedMessage } from "react-intl";

export default class authorIntro extends React.Component {
  render() {

    return(
      <div className="min-w-screen bg-blue flex flex-col items-center p-5 lg:p-10 overflow-hidden relative">

        <div className="w-2/3 max-w-12xl rounded-lg my-6 bg-background shadow-xl p-5 md:px-12 mx-auto text-dark relative flex xl:flex-row flex-col">
          <div className='m-auto md:pb-8 lg:px-12 pb-0'>
            <img src='/avatars/Natalia 2.png' className='m-auto'/>
          </div>

          <div className='m-auto md:text-left text-center'>
            <p className="leading-normal tracking-wide font-bold text-4xl pb-8 md:px-12 pt-6 md:pt-0">
              <FormattedMessage id='natalia' defaultMessage='default'/>

            </p>    
            <p className="leading-normal tracking-wide text-lg pb-8 md:px-12 pt-6 md:pt-0">
              <FormattedMessage id='natalia.description' defaultMessage='default'/>
            </p>  
          </div>
        </div>

        <div className="w-2/3 max-w-12xl rounded-lg my-6 bg-background shadow-xl p-5 md:px-12 mx-auto text-dark relative flex xl:flex-row flex-col">
          <div className='m-auto md:pb-6 lg:px-12 pb-0 xl:hidden block'>
            <img src='/avatars/Salvi1.png'  className='m-auto'/>
          </div>
          <div className='m-auto md:text-left text-center'>
            <p className="leading-normal tracking-wide font-bold text-4xl pb-8 md:px-12 pt-6 md:pt-0">
              <FormattedMessage id='salvi' defaultMessage='default'/>

            </p>    
            <p className="leading-normal tracking-wide text-lg pb-8 md:px-12 pt-6 md:pt-0">
              <FormattedMessage id='salvi.description' defaultMessage='default'/>
            </p>  
          </div>
          <div className='m-auto md:pb-6 lg:px-12 pb-0 hidden xl:block'>
            <img src='/avatars/Salvi1.png'  className='m-auto'/>
          </div>
        </div>

        <div className="w-2/3 max-w-12xl rounded-lg my-6 bg-background shadow-xl p-5 md:px-12 mx-auto text-dark relative flex xl:flex-row flex-col">
          <div className='m-auto md:pb-8 pb-0 lg:px-12'>
            <img src='/avatars/Paul2.png'  className='m-auto'/>
          </div>
          <div className='m-auto md:text-left text-center'>
            <p className="leading-normal tracking-wide font-bold text-4xl pb-8 md:px-12 pt-6 md:pt-0">
              <FormattedMessage id='paul' defaultMessage='default'/>

            </p>    
            <p className="leading-normal tracking-wide text-lg pb-8 md:px-12 pt-6 md:pt-0">
              <FormattedMessage id='paul.description' defaultMessage='default'/>
            </p>  
          </div>
        </div>

        </div>
    )
  }
}