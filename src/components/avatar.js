import React from "react";
import { FormattedMessage } from "react-intl";

export default class Avatar extends React.Component {
  render() {
    const { name, imagePath, imagePosition } = this.props;
    const padding = imagePosition ==='left'? 'pr-8' : 'pl-8';

    return(
      <div className={'float-' + imagePosition + ' ' + padding}>
        <img src={imagePath} className='h-64 object-cover' alt={name}/>
        <p className='leading-normal font-bold text-2xl text-center'>
          <FormattedMessage id={name} defaultMessage={'default'}/>
        </p>
      </div>
    )
  }
}