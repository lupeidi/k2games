import React from "react";
import { FormattedMessage } from "react-intl";
import Avatar from "../avatar";

export default class chapterFour extends React.Component {
  render() {

    return(
      <section className="text-dark w-full body-font pt-24">
        <div className="container px-5 pt-24 pb-12 mx-auto">
          <div className="lg:w-3/4 w-full mx-auto text-left">

            <Avatar
              name={'natalia'} 
              imagePath={'./avatars/Natalia1and4.png'}
              imagePosition={'right'}
            />
            <h1 className="pb-12 leading-normal font-bold text-6xl">
              <FormattedMessage id='chapterFour.title' defaultMessage='default'/>
            </h1> 

            <h2 className="pb-12 leading-normal font-bold text-2xl">
              <FormattedMessage id='chapterFour.subtitle1' defaultMessage='default'/>
            </h2> 
            <p className="leading-normal tracking-wide text-lg pb-8">
              <FormattedMessage id='chapterFour.1' defaultMessage='default'/>
            </p>
          </div>
        </div>
            
        <div className="min-w-screen bg-orange flex items-center p-5 lg:p-10 overflow-hidden relative">
          <div className="w-2/3 max-w-12xl rounded-lg bg-background shadow-xl p-5 lg:p-20 mx-auto text-dark relative md:text-left">

            <p className="leading-normal tracking-wide text-lg pb-8 px-12 text-center">
              <FormattedMessage id='chapterFour.2' defaultMessage='default'/>
            </p>    

            <button type="submit" className='block m-auto p-4 w-1/2'>
              <a href="/gameResources/Design Manual - Simulation Games.pdf" target="_blank" 
              className="block px-8 py-2 text-white rounded-lg font-bold leading-relaxed tracking-wider bg-orange">
                <FormattedMessage id={"openFile"} defaultMessage='default'/>
              </a>
            </button>
          </div>
        </div>

        <div className="container px-5 pt-12 pb-12 mx-auto">
          <div className="lg:w-3/4 w-full mx-auto text-left">
            <p className="leading-normal tracking-wide text-lg pb-8 pt-20">
              <FormattedMessage id='chapterFour.3' defaultMessage='default'/>
            </p>     
            <ul className='list-disc list-inside px-5 xl:px-18 pb-12'>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterFour.3.1' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterFour.3.2' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterFour.3.3' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterFour.3.4' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterFour.3.5' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterFour.3.6' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterFour.3.7' defaultMessage='default'/>
              </li>
            </ul> 

            <div className="max-w-md lg:max-w-2xl w-full mx-auto py-12">
              <img src='/chapter4conversation.svg' alt="people having a conversation" className='mx-auto'/>
            </div>

            <h2 className="py-12 leading-normal font-bold text-2xl">
              <FormattedMessage id='chapterFour.subtitle2' defaultMessage='default'/>
            </h2> 
            <p className="leading-normal tracking-wide text-lg pb-8">
              <FormattedMessage id='chapterFour.4' defaultMessage='default'/>
            </p>
            <ul className='list-disc list-inside px-5 xl:px-18 pb-12'>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterFour.4.1' defaultMessage='default'/>
                <a target="_blank" href='https://www.facebook.com/groups/k2gamescluj' className='hover:shadow-linkThick shadow-link '><FormattedMessage id='chapterFour.link1' defaultMessage='default'/></a>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterFour.4.2' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterFour.4.3' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterFour.4.4' defaultMessage='default'/>
              </li>
            </ul> 
            <p className="leading-normal tracking-wide text-lg pb-8">
              <FormattedMessage id='chapterFour.5' defaultMessage='default'/>
            </p>
          </div>
        </div>

        <div className="min-w-screen bg-orange flex items-center p-5 lg:p-10 overflow-hidden relative">
          <div className="w-2/3 max-w-12xl rounded-lg bg-background shadow-xl p-5 lg:p-20 mx-auto text-dark relative md:text-left">

            <p className="leading-normal tracking-wide text-lg pb-8 px-12 text-center">
              <FormattedMessage id='chapterFour.6' defaultMessage='default'/>
            </p>    

            <button type="submit" className='block m-auto p-4 w-1/2'>
              <a href="/gameResources/Design Manual - Board Games.pdf" target="_blank" 
              className="block px-8 py-2 text-white rounded-lg font-bold leading-relaxed tracking-wider bg-orange">
                <FormattedMessage id={"openFile"} defaultMessage='default'/>
              </a>
            </button>
          </div>
        </div>


        <div className="container px-5 pt-12 pb-8 mx-auto">
          <div className="lg:w-3/4 w-full mx-auto text-left">
            <p className="leading-normal tracking-wide text-lg pb-8 pt-20">
              <FormattedMessage id='chapterFour.7' defaultMessage='default'/>
            </p>            
            <ul className='list-disc list-inside px-5 xl:px-18 pb-12'>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterFour.7.1' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterFour.7.2' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterFour.7.3' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterFour.7.4' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterFour.7.5' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterFour.7.6' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterFour.7.7' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterFour.7.8' defaultMessage='default'/>
              </li>
            </ul> 
            <div className="max-w-md lg:max-w-2xl w-full mx-auto py-12">
              <img src='/chapter4.interaction.svg' alt="people interacting" className='mx-auto'/>
            </div>
          </div>
        </div>
        <div className="w-full px-5 py-12 mx-auto bg-yellow">
          <div className="lg:w-3/4 w-full mx-auto text-left">
            <p className="leading-normal italic tracking-wide text-lg text-center">
              <FormattedMessage id='chapterFour.closing' defaultMessage='default'/>
              <span className="font-bold">
                <FormattedMessage id='chapterFour.closingb' defaultMessage='default'/>
              </span>
            </p>
          </div>
        </div>

      </section>
    )
  }
}