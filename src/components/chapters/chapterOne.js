import React from "react";
import { FormattedMessage } from "react-intl";
import Avatar from "../avatar";
import Lens from "../lens";
import QuoteHero from "../quoteHero";

export default class chapterOne extends React.Component {
  render() {

    return(
      <section id="chapterOne" className="text-dark w-full body-font pt-24">
        <div className="container px-5 pt-24 pb-12 mx-auto">
          <div className="lg:w-3/4 w-full mx-auto text-left">

            <Avatar
              name={'salvi'} 
              imagePath={'./avatars/Salvi1.png'}
              imagePosition={'left'}
            />

            <h1 className="pb-12 leading-normal font-bold text-6xl">
              <FormattedMessage id='chapterOne.title' defaultMessage='default'/>
            </h1> 
            <h2 className="pb-12 leading-normal font-bold text-2xl">
              <FormattedMessage id='chapterOne.subtitle0' defaultMessage='default'/>
            </h2> 
            <p className="leading-normal tracking-wide text-lg pb-8">
              <FormattedMessage id='chapterOne.0' defaultMessage='default'/>
            </p>
            <p className="leading-normal tracking-wide text-lg pb-8">
              <FormattedMessage id='chapterOne.0b' defaultMessage='default'/>
            </p>            
            <p className="leading-normal tracking-wide text-lg pb-8">
              <FormattedMessage id='chapterOne.0c' defaultMessage='default'/>
            </p>

            <div className="max-w-md lg:max-w-2xl w-full mx-auto py-6">
              <img src='/chapter1connection.svg' alt="people connecting" className='mx-auto'/>
            </div>

            <h2 className="pb-12 leading-normal font-bold text-2xl pt-8">
              <FormattedMessage id='chapterOne.subtitle' defaultMessage='default'/>
            </h2> 
            <p className="leading-normal tracking-wide text-lg pb-8">
              <FormattedMessage id='chapterOne.1' defaultMessage='default'/>
              <a target="_blank" href='https://ec.europa.eu/programmes/erasmus-plus/node_en' className='shadow-link hover:shadow-linkThick'><FormattedMessage id='chapterOne.link' defaultMessage='default'/></a>
              <FormattedMessage id='chapterOne.1b' defaultMessage='default'/>
            </p>
            <p className="leading-normal tracking-wide text-lg pb-8">
              <FormattedMessage id='chapterOne.1c' defaultMessage='default'/>
            </p> 
            <ul className='list-decimal list-outside px-5 xl:px-18 pb-4'>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <p className='pb-4'><FormattedMessage id='chapterOne.example1.1' defaultMessage='default'/></p>
                <p className='pb-4'><FormattedMessage id='chapterOne.example1.2' defaultMessage='default'/></p>
                <p className='pb-4'><FormattedMessage id='chapterOne.example1.3' defaultMessage='default'/></p>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <p className='pb-4'><FormattedMessage id='chapterOne.example2.1' defaultMessage='default'/></p>
                <p className='pb-4'><FormattedMessage id='chapterOne.example2.2' defaultMessage='default'/></p>
                <p className='pb-4'><FormattedMessage id='chapterOne.example2.3' defaultMessage='default'/></p>
              </li>
            </ul>
          </div>

          <div className="max-w-md lg:max-w-2xl w-full mx-auto pb-12">
            <img src='/chapter1book.svg' alt="choosing where to put book" className='mx-auto'/>
          </div>

          <div className="lg:w-3/4 w-full mx-auto text-left">
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterOne.2' defaultMessage='default'/>
            </p>             
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterOne.3' defaultMessage='default'/>
            </p>             
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterOne.4' defaultMessage='default'/>
            </p>
            

            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterOne.5' defaultMessage='default'/>
            </p>
            <ul className='list-disc list-inside px-5 xl:px-18 pb-4'>
              <li className='leading-normal tracking-wide text-lg pb-2'>
                <FormattedMessage id='chapterOne.5.1' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-2'>
                <FormattedMessage id='chapterOne.5.2' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-2'>
                <FormattedMessage id='chapterOne.5.3' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-2'>
                <FormattedMessage id='chapterOne.5.4' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-2'>
                <FormattedMessage id='chapterOne.5.5' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-2'>
                <FormattedMessage id='chapterOne.5.6' defaultMessage='default'/>
              </li>
            </ul>
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterOne.5b' defaultMessage='default'/>
            </p>
            <p className="leading-normal tracking-wide text-lg pb-4">
              <span className='italic'>
                <FormattedMessage id='chapterOne.quote1' defaultMessage='default'/>
              </span>
              <span><FormattedMessage id='chapterOne.quote1author' defaultMessage='default'/></span>
            </p>

            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterOne.6' defaultMessage='default'/>
            </p>
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterOne.7' defaultMessage='default'/>
              <a href='/games' className='shadow-link hover:shadow-linkThick'><FormattedMessage id='chapterOne.link1' defaultMessage='default'/></a>
              <FormattedMessage id='chapterOne.7b' defaultMessage='default'/>
              <a target="_blank" href='https://www.facebook.com/groups/k2gamescluj' className='shadow-link hover:shadow-linkThick'><FormattedMessage id='chapterOne.link2' defaultMessage='default'/></a>
            </p>
            <div className="max-w-md lg:max-w-2xl w-full mx-auto py-24">
              <img src='/chapter1quote.svg' alt="quote" className='mx-auto'/>
            </div>
            <p className="leading-normal tracking-wide text-lg pb-4 pt-12">
              <FormattedMessage id='chapterOne.8' defaultMessage='default'/>
            </p>
          </div>
        </div>
        
          <QuoteHero
            author={'chapterOne.quote2author'}
            secondReference={'chapterOne.quote2referenceb'}
            reference={'chapterOne.quote2reference'}
            quote={'chapterOne.quote2'}
            imagePath={'/avatars/Andrea.jpg'}
            imageLeft={true}
          />

          <QuoteHero
            author={'chapterOne.quote3author'}
            reference={'chapterOne.quote3reference'}
            secondReference={'chapterOne.quote3referenceb'}
            quote={'chapterOne.quote3'}
            imagePath={'/avatars/Laura.jpg'}
            imageLeft={false}
          />

          <QuoteHero
            author={'chapterOne.quote4author'}
            reference={'chapterOne.quote4reference'}
            secondReference={'chapterOne.quote4referenceb'}
            quote={'chapterOne.quote4'}
            imagePath={'/avatars/FrancescoB.jpg'}
            imageLeft={true}
          />

        <Lens
        textComponent={
          <>  
          <ul className='list-disc list-inside px-5 xl:px-18'>
            <li className='leading-normal tracking-wide text-lg pb-4'>
              <FormattedMessage id='chapterOne.lens1' defaultMessage='default'/>
            </li>           
            <li className='leading-normal tracking-wide text-lg pb-4'>
              <FormattedMessage id='chapterOne.lens2' defaultMessage='default'/>
            </li>  
            <li className='leading-normal tracking-wide text-lg pb-4'>
              <FormattedMessage id='chapterOne.lens3' defaultMessage='default'/>
            </li>  
            <li className='leading-normal tracking-wide text-lg'>
              <FormattedMessage id='chapterOne.lens4' defaultMessage='default'/>
              <ul className='list-disc list-outside px-5 xl:px-18 pl-12'>
                <li className='leading-normal tracking-wide text-lg pb-2'>
                  <FormattedMessage id='chapterOne.lens4a' defaultMessage='default'/>
                </li>  
                <li className='leading-normal tracking-wide text-lg pb-2'>
                  <FormattedMessage id='chapterOne.lens4b' defaultMessage='default'/>
                </li>  
                <li className='leading-normal tracking-wide text-lg'>
                  <FormattedMessage id='chapterOne.lens4c' defaultMessage='default'/>
                </li>  
              </ul>
            </li>          
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterOne.lens5' defaultMessage='default'/>
                <ul className='list-disc list-outside px-5 xl:px-18 pb-4 pl-12'>
                  <li className='leading-normal tracking-wide text-lg pb-2'>
                    <FormattedMessage id='chapterOne.lens5a' defaultMessage='default'/>
                  </li>  
                  <li className='leading-normal tracking-wide text-lg pb-2'>
                    <FormattedMessage id='chapterOne.lens5b' defaultMessage='default'/>
                  </li>  
                </ul>
              </li> 
            </ul>
          </>
        }
        />

        <div className="w-full px-5 py-12 mx-auto bg-yellow">
          <div className="lg:w-3/4 w-full mx-auto text-center">
            <p className="leading-normal italic tracking-wide text-lg pb-4">
              <FormattedMessage id='guide.intro2.closing' defaultMessage='default'/>
              <span className="font-bold">
                <FormattedMessage id='guide.reading' defaultMessage='default'/>
              </span>
            </p> 
          </div>
        </div>
      </section>
    )
  }
}