import React from "react";
import { FormattedMessage } from "react-intl";
import QuoteHero from "../quoteHero";
import Lens from "../lens";
import Avatar from "../avatar";
import ExternalLinks from '../externalLinks';

export default class chapterThree extends React.Component {
  render() {

    return(
      <section id='chapterThree' className="text-dark w-full body-font pt-24">
        <div className="container px-5 pt-24 pb-12 mx-auto">
          <div className="lg:w-3/4 w-full mx-auto text-left">

            <Avatar
              name={'natalia'} 
              imagePath={'./avatars/Natalia 2.png'}
              imagePosition={'left'}
            />

            <h1 className="pb-12 leading-normal font-bold text-6xl">
              <FormattedMessage id='chapterThree.title' defaultMessage='default'/>
            </h1> 


            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterThree.0' defaultMessage='default'/>
              <a href={'/project'}  className='shadow-link hover:shadow-linkThick'><FormattedMessage id='chapterThree.link' defaultMessage='default'/></a>
              <FormattedMessage id='chapterThree.0b' defaultMessage='default'/>
            </p>
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterThree.0c' defaultMessage='default'/>
              <a href={'#chapterOne'}  className='shadow-link hover:shadow-linkThick'><FormattedMessage id='chapterThree.link2' defaultMessage='default'/></a>
              <FormattedMessage id='chapterThree.0d' defaultMessage='default'/>
              <a href={'#chapterTwo'}  className='shadow-link hover:shadow-linkThick'><FormattedMessage id='chapterThree.link3' defaultMessage='default'/></a>
            </p>

            <h2 className="py-12 leading-normal font-bold text-2xl lg:pt-20">
              <FormattedMessage id='chapterThree.subtitle1' defaultMessage='default'/>
            </h2> 
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterThree.1' defaultMessage='default'/>
              <span className='italic'><FormattedMessage id='chapterThree.1b' defaultMessage='default'/></span>
            </p>


            <p className="leading-normal tracking-wide text-lg pb-2">
              <FormattedMessage id='chapterThree.example1' defaultMessage='default'/>
            </p>
            <p className="leading-normal tracking-wide text-lg pb-2">
              <FormattedMessage id='chapterThree.example2' defaultMessage='default'/>
            </p>
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterThree.example3' defaultMessage='default'/>
            </p>

            <p className="leading-normal tracking-wide text-lg pb-2">
              <FormattedMessage id='chapterThree.2' defaultMessage='default'/>
            </p>
            <p className="leading-normal tracking-wide text-lg pb-2">
              <a href={'/games/pioneer-city'} className='shadow-link hover:shadow-linkThick'><FormattedMessage id='chapterThree.link5' defaultMessage='default'/></a>
            </p>
            <p className="leading-normal tracking-wide text-lg pb-2">
              <a href={'/games/recycling-party'} className='shadow-link hover:shadow-linkThick'><FormattedMessage id='chapterThree.link6' defaultMessage='default'/></a>
            </p>


            <ExternalLinks
              imagePath={'/chapter3ordering.svg'}
              imageAlt={'ordering'}
              description2={'chapterThree.1c'}
              link2={'http://sustainerasmus.eu/wp/interactive-boardgame-based-learning-environments-for-decision-makers-training-in-managerial-education/'}
              description1={'chapterThree.3'}
              link1={'https://en.wikipedia.org/wiki/Board_game'}
              description3={'chapterThree.4'}
              link3={'https://www.youtube.com/watch?v=y2dIEbNtNsg'}
            />

            <h2 className="py-12 leading-normal font-bold text-2xl">
              <FormattedMessage id='chapterThree.subtitle2' defaultMessage='default'/>
            </h2>
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterThree.6' defaultMessage='default'/>
            </p>             
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterThree.7' defaultMessage='default'/>
            </p>             
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterThree.8' defaultMessage='default'/>
            </p>

            <h2 className="py-12 leading-normal font-bold text-2xl">
              <FormattedMessage id='chapterThree.subtitle3' defaultMessage='default'/>
            </h2>
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterThree.9' defaultMessage='default'/>
            </p>             
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterThree.10' defaultMessage='default'/>
            </p>
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterThree.11' defaultMessage='default'/>
            </p>

            <div className="max-w-md lg:max-w-2xl w-full mx-auto p-20">
              <img src='/chapter3imagination.svg' alt="imagination" className='mx-auto'/>
            </div> 

            <h2 className="py-12 leading-normal font-bold text-2xl">
              <FormattedMessage id='chapterThree.subtitle4' defaultMessage='default'/>
            </h2>
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterThree.12' defaultMessage='default'/>
            </p>
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterThree.13' defaultMessage='default'/>
            </p>
            <ul className='list-disc list-inside px-5 xl:px-18 pb-4'>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterThree.13.1' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterThree.13.2' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterThree.13.3' defaultMessage='default'/>
              </li>
            </ul>

            <ExternalLinks
              imagePath={'/chapter3choice.svg'}
              imageAlt={'looking for directions'}
              description1={'chapterThree.link9'}
              link1={'https://en.wikipedia.org/wiki/Roleplay_simulation'}
              description2={'chapterThree.link10'}
              link2={'https://crisp-berlin.org/fileadmin/ABOUT/Organisation/Downloads/Public_Relations/CRISP_Abstract_2019_EN.pdf'}
              description3={'chapterThree.link11'}
              link3={'https://www.ted.com/talks/john_hunter_teaching_with_the_world_peace_game'}
            />

            <h2 className="pb-12 leading-normal font-bold text-2xl pt-16">
              <FormattedMessage id='chapterThree.subtitle5' defaultMessage='default'/>
            </h2>
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterThree.15' defaultMessage='default'/>
            </p>
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterThree.16' defaultMessage='default'/>
            </p>
            <ul className='list-disc list-inside px-5 xl:px-18 pb-4'>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterThree.16.1' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterThree.16.2' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterThree.16.3' defaultMessage='default'/>
                <a href={'#chapterTwo'} className='shadow-link hover:shadow-linkThick'><FormattedMessage id='chapterThree.16.3link' defaultMessage='default'/> </a> 
              </li>
            </ul>

            <p className="leading-normal tracking-wide text-lg pb-4 pt-12">
              <FormattedMessage id='chapterThree.17' defaultMessage='default'/>
            </p>
          </div>
        </div>

        <QuoteHero
          author={'chapterThree.quote1author'}
          reference={'chapterThree.quote1reference'}
          secondReference={'chapterThree.quote1referenceb'}
          quote={'chapterThree.quote1'}
          imagePath={'/avatars/Natalia.jpg'}
          imageLeft={false}
        />

        <QuoteHero
          author={'chapterThree.quote2author'}
          reference={'chapterThree.quote2reference'}
          secondReference={'chapterThree.quote2referenceb'}
          quote={'chapterThree.quote2'}
          imagePath={'/avatars/Antonis.jpg'}
          imageLeft={true}
        />  

        <Lens
          textComponent={
            <> 
              <p className="leading-normal tracking-wide text-lg pb-4">
                <FormattedMessage id='chapterThree.lens1' defaultMessage='default'/>
              </p>    
              <ul className='list-disc list-outside px-5 xl:px-18 pb-4 pl-12'>
                <li className='leading-normal tracking-wide text-lg pb-2'>
                  <FormattedMessage id='chapterThree.lensquestion1' defaultMessage='default'/>
                </li>  
                <li className='leading-normal tracking-wide text-lg pb-2'>
                  <FormattedMessage id='chapterThree.lensquestion2' defaultMessage='default'/>
                  <a href={'/project'} className='shadow-link hover:shadow-linkThick'><FormattedMessage id='chapterThree.lenslink' defaultMessage='default'/> </a> 
                </li>  
              </ul>
              <p className="leading-normal tracking-wide text-lg pb-4">
                <FormattedMessage id='chapterThree.lens2' defaultMessage='default'/>
                <a href={'#chapterOne'} className='shadow-link hover:shadow-linkThick'><FormattedMessage id='chapterThree.lenslink2' defaultMessage='default'/> </a> 
                <FormattedMessage id='chapterThree.lens2b' defaultMessage='default'/>
                <a href={'#chapterX'} className='shadow-link hover:shadow-linkThick'><FormattedMessage id='chapterThree.lenslink3' defaultMessage='default'/> </a> 
                <FormattedMessage id='chapterThree.lens2c' defaultMessage='default'/>
                <a target='_blank' href={'https://www.facebook.com/groups/k2gamescluj'} className='shadow-link hover:shadow-linkThick'><FormattedMessage id='chapterThree.lenslink3' defaultMessage='default'/> </a> 
                <FormattedMessage id='chapterThree.lens2d' defaultMessage='default'/>
              </p>   
            </>
          }
          secondaryText={
            <>
              <p className="leading-normal tracking-wide text-lg pb-4">
                <FormattedMessage id='chapterThree.lens3' defaultMessage='default'/>
              </p> 
              <ul className='list-disc list-outside px-5 xl:px-18 pb-4 pl-12'>
                <li className='leading-normal tracking-wide text-lg pb-2'>
                  <p>
                    <FormattedMessage id='chapterThree.lensquestion3' defaultMessage='default'/>
                  </p>
                  <p>
                    <span className='font-bold'><FormattedMessage id='chapterThree.lensbold' defaultMessage='default'/></span>
                    <FormattedMessage id='chapterThree.lensquestion3b' defaultMessage='default'/>
                  </p>
                </li>  
                <li className='leading-normal tracking-wide text-lg pb-2'>
                  <p>
                    <FormattedMessage id='chapterThree.lensquestion4' defaultMessage='default'/>
                  </p>
                  <p>
                    <span className='font-bold'><FormattedMessage id='chapterThree.lensbold' defaultMessage='default'/></span>
                    <FormattedMessage id='chapterThree.lensquestion4b' defaultMessage='default'/>
                  </p>
                </li> 
                <li className='leading-normal tracking-wide text-lg pb-2'>
                    <FormattedMessage id='chapterThree.lensquestion5' defaultMessage='default'/>
                </li> 
                <li className='leading-normal tracking-wide text-lg pb-2'>
                  <p>
                    <FormattedMessage id='chapterThree.lensquestion6' defaultMessage='default'/>
                  </p>
                  <p>
                    <span className='font-bold'><FormattedMessage id='chapterThree.lensbold' defaultMessage='default'/></span>
                    <FormattedMessage id='chapterThree.lensquestion6b' defaultMessage='default'/>
                  </p>
                </li> 
                <li className='leading-normal tracking-wide text-lg pb-2'>
                  <p>
                    <FormattedMessage id='chapterThree.lensquestion7' defaultMessage='default'/>
                  </p>
                  <p>
                    <span className='font-bold'><FormattedMessage id='chapterThree.lensbold' defaultMessage='default'/></span>
                    <FormattedMessage id='chapterThree.lensquestion7b' defaultMessage='default'/>
                    <a href={'#chapterFour'} className='shadow-link hover:shadow-linkThick'><FormattedMessage id='chapterThree.lensquestion7.link1' defaultMessage='default'/></a>
                    <FormattedMessage id='chapterThree.lensquestion7c' defaultMessage='default'/>
                    <a target="_blank" href={'https://www.facebook.com/groups/k2gamescluj'} className='shadow-link hover:shadow-linkThick'><FormattedMessage id='chapterThree.lensquestion7.link2' defaultMessage='default'/></a>
                  </p>
                </li> 
              </ul>
            </>
          }
        />

        <div className="w-full px-5 py-12 mx-auto bg-yellow">
          <div className="lg:w-3/4 w-full mx-auto text-center">
            <p className="leading-normal italic tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterThree.closing' defaultMessage='default'/>
              <span className="font-bold">
                <FormattedMessage id='guide.reading' defaultMessage='default'/>
              </span>
            </p> 
          </div>
        </div>

      </section>
    )
  }
}