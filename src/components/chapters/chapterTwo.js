import React from "react";
import { FormattedMessage } from "react-intl";
import QuoteHero from "../quoteHero";
import Lens from "../lens";
import Avatar from "../avatar";
import ExternalLinks from '../externalLinks';

export default class chapterTwo extends React.Component {
  render() {

    return(
      <section id='chapterTwo' className="text-dark w-full body-font pt-24">
        <div className="container px-5 pt-24 pb-12 mx-auto">
          <div className="lg:w-3/4 w-full mx-auto text-left">
            <Avatar
              name={'natalia'} 
              imagePath={'./avatars/Natalia3.png'}
              imagePosition={'right'}
            />
            <h1 className="pb-12 leading-normal font-bold text-6xl">
              <FormattedMessage id='chapterTwo.title' defaultMessage='default'/>
            </h1> 

            <p className="leading-normal tracking-wide text-lg pb-8">
              <FormattedMessage id='chapterTwo.1' defaultMessage='default'/>
            </p>
            <p className="leading-normal tracking-wide text-lg pb-8">
              <FormattedMessage id='chapterTwo.2' defaultMessage='default'/>
            </p>     

            <ExternalLinks
              imagePath={'/chapter2progress.svg'}
              imageAlt={'person making progress'}
              description1={'chapterTwo.5'}
              link1={'https://www.youtube.com/watch?v=ZNwDAKAvhLE'}
              description2={'chapterTwo.5b'}
              link2={'https://www.ted.com/talks/doug_snyder_small_steps_for_a_big_change'}
              description3={'chapterTwo.5c'}
              link3={'https://www.earlytorise.com/why-you-should-start-small-2/'}
            />

            <p className="leading-normal tracking-wide text-lg pb-8 pt-12">
              <FormattedMessage id='chapterTwo.3' defaultMessage='default'/>
            </p>            


            <p className="leading-normal tracking-wide text-lg pb-8">
              <FormattedMessage id='chapterTwo.6' defaultMessage='default'/>
            </p>
          </div>
        </div>

        <QuoteHero
          author={'chapterTwo.quote1author'}
          reference={'chapterTwo.quote1reference'}
          secondReference={'chapterTwo.quote1referenceb'}
          quote={'chapterTwo.quote1'}
          imagePath={'/avatars/Dhyan.svg'}
          imageLeft={false}
        />

        <QuoteHero
          author={'chapterTwo.quote2author'}
          reference={'chapterTwo.quote2reference'}
          secondReference={'chapterTwo.quote2referenceb'}
          quote={'chapterTwo.quote2'}
          imagePath={'/avatars/Aiste.jpg'}
          imageLeft={true}
        />

        <Lens
          textComponent={
            <>
              <p className="leading-normal tracking-wide text-lg pb-8">
                <FormattedMessage id='chapterTwo.lens1' defaultMessage='default'/>
              </p>
              <p className="leading-normal tracking-wide text-lg pb-8">
                <FormattedMessage id='chapterTwo.lens2' defaultMessage='default'/>
              </p>
              <p className="leading-normal tracking-wide text-lg">
                <FormattedMessage id='chapterTwo.lens3' defaultMessage='default'/>
              </p>
              <ul className='list-disc list-inside px-5 xl:px-18 pb-4'>
                <li className='leading-normal tracking-wide text-lg pb-2'>
                <FormattedMessage id='chapterTwo.lensquestion1' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-2'>
                <FormattedMessage id='chapterTwo.lensquestion2' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-2'>
                <FormattedMessage id='chapterTwo.lensquestion3' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-2'>
                <FormattedMessage id='chapterTwo.lensquestion4' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-2'>
                <FormattedMessage id='chapterTwo.lensquestion5' defaultMessage='default'/>
              </li>
            </ul>
            </>
          }
        />

        <div className="w-full px-5 py-12 mx-auto bg-yellow">
          <div className="lg:w-3/4 w-full mx-auto text-center">
            <p className="leading-normal italic tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterTwo.closing' defaultMessage='default'/>
              <span className="font-bold">
                <FormattedMessage id='guide.reading' defaultMessage='default'/>
              </span>
            </p> 
          </div>
        </div>
      </section>
    )
  }
}