import React from "react";
import { FormattedMessage } from "react-intl";
import Avatar from "../avatar";
import Lens from "../lens";
import QuoteHero from "../quoteHero";

export default class chapterX extends React.Component {
  render() {

    return(
      <section className="text-dark w-full body-font pt-24">
        <div className="container px-5 pt-24 pb-12 mx-auto">
          <div className="lg:w-3/4 w-full mx-auto text-left">

            <Avatar
              name={'paul'} 
              imagePath={'./avatars/Paul2.png'}
              imagePosition={'left'}
            />
            <h1 className="pb-12 leading-normal font-bold text-6xl">
              <FormattedMessage id='chapterX.title' defaultMessage='default'/>
            </h1> 
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterX.1' defaultMessage='default'/>
            </p>
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterX.2' defaultMessage='default'/>
            </p>
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterX.3' defaultMessage='default'/>
            </p>
            <ul className='list-disc list-inside px-5 xl:px-18 pb-4'>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterX.3.1' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterX.3.2' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterX.3.3' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterX.3.4' defaultMessage='default'/>
              </li>
            </ul>
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterX.4' defaultMessage='default'/>
            </p>

            <div className="max-w-md lg:max-w-2xl w-full mx-auto py-12">
              <img src='/chapterXtogheter.svg' alt="people togheter" className='mx-auto'/>
            </div>

            <h2 className="py-12 leading-normal font-bold text-2xl">
              <FormattedMessage id='chapterX.subtitle1' defaultMessage='default'/>
            </h2> 
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterX.5' defaultMessage='default'/>
            </p>


            <h3 className="leading-normal tracking-wide font-bold text-xl pb-4 pt-8">
              <FormattedMessage id='chapterX.step1title' defaultMessage='default'/>
            </h3>   
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterX.step1' defaultMessage='default'/>
            </p>             

            <h3 className="leading-normal tracking-wide font-bold text-xl pb-4 pt-8">
              <FormattedMessage id='chapterX.step2title' defaultMessage='default'/>
            </h3>   
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterX.step2' defaultMessage='default'/>
            </p>    

            <h3 className="leading-normal tracking-wide font-bold text-xl pb-4 pt-8">
              <FormattedMessage id='chapterX.step3title' defaultMessage='default'/>
            </h3>   
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterX.step3' defaultMessage='default'/>
            </p> 

            <div className="max-w-md lg:max-w-2xl w-full mx-auto py-12">
              <img src='/chapterXtalk.svg' alt="people talking" className='mx-auto'/>
            </div>

            <h2 className="py-12 leading-normal font-bold text-2xl">
              <FormattedMessage id='chapterX.subtitle2' defaultMessage='default'/>
            </h2>    
            <h3 className="leading-normal tracking-wide font-bold text-xl pb-4 pt-8">
              <FormattedMessage id='chapterX.step4title' defaultMessage='default'/>
            </h3>   
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterX.step4' defaultMessage='default'/>
            </p> 
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterX.step4b' defaultMessage='default'/>
            </p> 
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterX.step4.1' defaultMessage='default'/>
            </p> 
            <ul className='list-disc list-inside px-5 xl:px-18 pb-4'>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterX.step4.1.1' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterX.step4.1.2' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='chapterX.step4.1.3' defaultMessage='default'/>
              </li>
            </ul>
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterX.step4.2' defaultMessage='default'/>
            </p> 
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterX.step4.3' defaultMessage='default'/>
            </p> 

            <h3 className="leading-normal tracking-wide font-bold text-xl pb-4 pt-8">
              <FormattedMessage id='chapterX.step5title' defaultMessage='default'/>
            </h3>   
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterX.step5' defaultMessage='default'/>
            </p> 

            <h3 className="leading-normal tracking-wide font-bold text-xl pb-4 pt-8">
              <FormattedMessage id='chapterX.step6title' defaultMessage='default'/>
            </h3>   
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterX.step6' defaultMessage='default'/>
            </p> 

            <h3 className="leading-normal tracking-wide font-bold text-xl pb-4 pt-8">
              <FormattedMessage id='chapterX.step7title' defaultMessage='default'/>
            </h3>   
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterX.step7' defaultMessage='default'/>
            </p> 

            <h3 className="leading-normal tracking-wide font-bold text-xl pb-4 pt-8">
              <FormattedMessage id='chapterX.step8title' defaultMessage='default'/>
            </h3>   
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='chapterX.step8' defaultMessage='default'/>
            </p> 
            
            <div className="max-w-md lg:max-w-2xl w-full mx-auto py-12">
              <img src='/chapterXmessage.svg' alt="person sending a message" className='mx-auto'/>
            </div>
          </div>
        </div>
        
        <Lens
          textComponent={
            <> 
              <ul className='list-disc list-outside px-5 xl:px-18'>
                <li className='leading-normal tracking-wide text-lg pb-4'>
                  <FormattedMessage id='chapterX.lens1' defaultMessage='default'/>
                </li>           
                <li className='leading-normal tracking-wide text-lg pb-4'>
                  <FormattedMessage id='chapterX.lens2' defaultMessage='default'/>
                </li>  
                <li className='leading-normal tracking-wide text-lg pb-4'>
                  <FormattedMessage id='chapterX.lens3' defaultMessage='default'/>
                </li>  
                <li className='leading-normal tracking-wide text-lg pb-4'>
                  <FormattedMessage id='chapterX.lens4' defaultMessage='default'/>
                </li>                
                <li className='leading-normal tracking-wide text-lg pb-4'>
                  <FormattedMessage id='chapterX.lens5' defaultMessage='default'/>
                </li>                
                <li className='leading-normal tracking-wide text-lg pb-4'>
                  <FormattedMessage id='chapterX.lens6' defaultMessage='default'/>
                </li>                
                <li className='leading-normal tracking-wide text-lg pb-4'>
                  <FormattedMessage id='chapterX.lens7' defaultMessage='default'/>
                </li>  
              </ul>
            </>
          }
        />

        <QuoteHero
          author={'chapterX.quote2author'}
          secondReference={'chapterX.quote2referenceb'}
          reference={'chapterX.quote2reference'}
          quote={'chapterX.quote2'}
          imagePath={'/avatars/Erzso.png'}
          imageLeft={false}
        />
        <QuoteHero
          author={'chapterX.quote1author'}
          secondReference={'chapterX.quote1referenceb'}
          reference={'chapterX.quote1reference'}
          quote={'chapterX.quote1'}
          imagePath={'/avatars/Salvi2.png'}
          imageLeft={true}
        />

        {/* <section className="text-dark body-font border-t border-b  border-gray-200">
          <div className="container py-24 px-5 m-auto">
            <div className="lg:w-3/4 w-full m-auto text-center">

              <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" className="inline-block w-8 h-8 text-orange mb-8" viewBox="0 0 975.036 975.036">
                <path d="M925.036 57.197h-304c-27.6 0-50 22.4-50 50v304c0 27.601 22.4 50 50 50h145.5c-1.9 79.601-20.4 143.3-55.4 191.2-27.6 37.8-69.399 69.1-125.3 93.8-25.7 11.3-36.8 41.7-24.8 67.101l36 76c11.6 24.399 40.3 35.1 65.1 24.399 66.2-28.6 122.101-64.8 167.7-108.8 55.601-53.7 93.7-114.3 114.3-181.9 20.601-67.6 30.9-159.8 30.9-276.8v-239c0-27.599-22.401-50-50-50zM106.036 913.497c65.4-28.5 121-64.699 166.9-108.6 56.1-53.7 94.4-114.1 115-181.2 20.6-67.1 30.899-159.6 30.899-277.5v-239c0-27.6-22.399-50-50-50h-304c-27.6 0-50 22.4-50 50v304c0 27.601 22.4 50 50 50h145.5c-1.9 79.601-20.4 143.3-55.4 191.2-27.6 37.8-69.4 69.1-125.3 93.8-25.7 11.3-36.8 41.7-24.8 67.101l35.9 75.8c11.601 24.399 40.501 35.2 65.301 24.399z"></path>
              </svg>
              <div className="leading-relaxed text-lg">
                <p className="pb-4">
                  <FormattedMessage id={'chapterX.quote1'} defaultMessage='default'/>
                </p>
                <p className="pb-4">
                  <FormattedMessage id={'chapterX.quote1b'} defaultMessage='default'/>
                </p>
                <p className="pb-4">
                  <FormattedMessage id={'chapterX.quote1c'} defaultMessage='default'/>
                </p>
              </div>
              <span className="inline-block h-1 w-10 rounded bg-orange mt-8 mb-6"></span>
              <h2 className="text-gray-900 font-medium title-font tracking-wider text-sm">              
                <FormattedMessage id={'chapterX.quote1author'} defaultMessage='default'/>
              </h2>
                <p className="text-gray-500">
                  <FormattedMessage id={'chapterX.quote1reference'} defaultMessage='default'/>
                </p>

                <p className="text-gray-500">
                  <FormattedMessage id={'chapterX.quote1referenceb'} defaultMessage='default'/>
                </p>

            </div>
          </div>
        </section> */}
      </section>
    )
  }
}