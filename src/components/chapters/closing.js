import React from "react";
import { FormattedMessage } from "react-intl";

export default class closing extends React.Component {  
  render() {

    return(
      <section className="text-dark w-full body-font">
        <div className="max-w-md lg:max-w-2xl w-full mx-auto py-12">
          <img src='/ithaka.svg' alt="journeyz" className='mx-auto'/>
        </div>

        <div className="container px-5 pt-24 pb-12 mx-auto">
          <div className="lg:w-3/4 w-full mx-auto text-left">


            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='closing.1' defaultMessage='default'/>
            </p>
            <p className="leading-normal tracking-wide text-lg pb-12">
              <FormattedMessage id='closing.2' defaultMessage='default'/>
            </p>
          </div>
        </div>
        
        {/* Ithaka */}
        <div className="min-w-screen min-h-screen bg-blue flex flex-row items-center p-5 lg:p-10 overflow-hidden relative">
          <div className="w-full md:w-1/2 max-w-12xl rounded-lg flex flex-col items-center bg-background shadow-xl p-1 sm:p-5 lg:p-20 mx-auto text-dark relative md:text-left">
            <div className='text-left'>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka1' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka2' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka3' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka4' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka5' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka6' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka7' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka8' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka9' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka10' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka11' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka12' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-10'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka13' defaultMessage='default'/>
                </p>
              </div>

              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka14' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka15' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka16' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka17' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka18' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka19' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka20' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka21' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka22' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka23' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-10'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka24' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka25' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka26' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka27' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka28' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka29' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka30' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-10'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka31' defaultMessage='default'/>
                </p>
              </div>
              
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka32' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka33' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-10'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka34' defaultMessage='default'/>
                </p>
              </div>

              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka35' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-2'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka36' defaultMessage='default'/>
                </p>
              </div>
              <div className='pb-10'>
                <p className="leading-normal tracking-wide text-lg">
                  <FormattedMessage id='closing.ithaka37' defaultMessage='default'/>
                </p>
              </div>

              <div className='text-right'>
                <div className='pb-2'>
                  <p className="leading-normal font-bold italic tracking-wide text-xl">
                    <FormattedMessage id='closing.ithaka.title' defaultMessage='default'/>
                  </p>
                </div>
                <div className='pb-2'>
                  <p className="leading-normal italic tracking-wide text-xl">
                    <FormattedMessage id='closing.ithaka.author' defaultMessage='default'/>
                  </p>
                </div>
                <div className='pb-2'>
                  <p className="leading-normal italic tracking-wide text-xl">
                    <FormattedMessage id='closing.ithaka.translation' defaultMessage='default'/>
                  </p>
                </div>
              </div>
            </div>

          </div>
        </div>

        <div className="min-w-screen min-h-screen flex flex-row bg-yellow items-center p-5 lg:p-10 overflow-hidden relative">
          <div className="w-full md:w-4/5 xl:w-2/3 max-w-12xl rounded-lg flex flex-col items-center sm:p-5 lg:p-20 mx-auto text-dark relative md:text-left">
         
            <p className="leading-normal tracking-wide text-xl py-20">
              <FormattedMessage id='closing.3' defaultMessage='default'/>
            </p>

            <div className="max-w-md lg:max-w-2xl w-full mx-auto">
              <img src='/team.svg' alt="people in a team" className='mx-auto'/>
            </div>

            <div className="container px-5 pt-32 pb-12 mx-auto">

              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-8">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name1'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description1' defaultMessage='default'/>
                </p>
              </div>
              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-8">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name2'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description2' defaultMessage='default'/>
                </p>
              </div>
              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-8">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name3'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description3' defaultMessage='default'/>
                </p>
              </div>
              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-8">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name4'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description4' defaultMessage='default'/>
                </p>
              </div>
              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-8">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name5'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description5' defaultMessage='default'/>
                </p>
              </div>
              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-8">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name6'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description6' defaultMessage='default'/>
                </p>
              </div>
              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-8">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name7'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description7' defaultMessage='default'/>
                </p>
              </div>
              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-8">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name8'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description8' defaultMessage='default'/>
                </p>
              </div>
              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-8">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name9'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description9' defaultMessage='default'/>
                </p>
              </div>
              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-8">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name10'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description10' defaultMessage='default'/>
                </p>
              </div>
              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-8">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name11'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description11' defaultMessage='default'/>
                </p>
              </div>
              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-8">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name12'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description12' defaultMessage='default'/>
                </p>
              </div>
              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-8">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name13'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description13' defaultMessage='default'/>
                </p>
              </div>
              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-8">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name14'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description14' defaultMessage='default'/>
                </p>
              </div>
              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-8">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name15'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description15' defaultMessage='default'/>
                </p>
              </div>
              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-8">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name16'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description16' defaultMessage='default'/>
                </p>
              </div>
              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-8">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name17'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description17' defaultMessage='default'/>
                </p>
              </div>
              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-8">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name18'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description18' defaultMessage='default'/>
                </p>
              </div>
              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-8">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name19'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description19' defaultMessage='default'/>
                </p>
              </div>
              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-8">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name20'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description20' defaultMessage='default'/>
                </p>
              </div>
              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-8">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name21'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description21' defaultMessage='default'/>
                </p>
              </div>
              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-8">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name22'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description22' defaultMessage='default'/>
                </p>
              </div>
              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-8">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name23'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description23' defaultMessage='default'/>
                </p>
              </div>
              <div className="lg:w-3/4 w-full mx-auto text-left">
                <p className="leading-normal tracking-wide text-lg pb-12">
                  <span className='font-bold'><FormattedMessage id='closing.credit.name24'defaultMessage='default'/></span>
                  <FormattedMessage id='closing.credit.description24' defaultMessage='default'/>
                </p>
              </div>

              <div className="lg:w-3/4 w-full mx-auto text-left pt-6">
                <p className="leading-normal tracking-wide font-bold text-lg pb-20">
                  <a target="_blank" href='https://www.facebook.com/groups/k2gamescluj'>
                    <FormattedMessage id='closing.group' defaultMessage='default'/>&rarr; </a>
                </p>
              </div>
            </div>            
          </div>

        </div>

            
      </section>
    )
  }
}