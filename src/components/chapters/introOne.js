import React from "react";
import { FormattedMessage } from "react-intl";
import ExternalLinks from '../externalLinks';
import Lens from "../lens";
import QuoteHero from '../quoteHero';
import Avatar from '../avatar';
import AuthorIntro from "../authorIntro";


export default class Intro extends React.Component {
  render() {

    return(
      <section id='intro' className="text-dark w-full body-font">
        <div className="container px-5 py-24 mx-auto">
          <div className="lg:w-3/4 w-full mx-auto text-left">

            <h1 className="pb-12 leading-normal font-bold text-6xl">
              <FormattedMessage id='guide.text' defaultMessage='default'/>
            </h1>
            <p className="leading-normal font-bold tracking-wide text-lg pb-4">
              <FormattedMessage id='guide.intro1.1' defaultMessage='default'/>
            </p> 
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='guide.intro1.2' defaultMessage='default'/>
            </p>             
            <p className="leading-normal font-bold tracking-wide text-lg pb-4">
              <FormattedMessage id='guide.intro1.0' defaultMessage='default'/>
            </p> 
          </div>
        </div>

        <AuthorIntro/>

        <div className="container px-5 py-24 mx-auto">
          <div className="lg:w-3/4 w-full mx-auto text-left">

            <Avatar
              name={'natalia'} 
              imagePath={'./avatars/Natalia1and4.png'}
              imagePosition={'right'}
            />
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='guide.intro1.3' defaultMessage='default'/>
            </p>             
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='guide.intro1.4' defaultMessage='default'/>
            </p>             
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='guide.intro1.5' defaultMessage='default'/>
            </p>             
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='guide.intro1.6' defaultMessage='default'/>
            </p>             
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='guide.intro1.7' defaultMessage='default'/>
            </p>             
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='guide.intro1.8' defaultMessage='default'/>
            </p> 
            <p className="leading-normal font-bold tracking-wide text-lg pb-4">
              <FormattedMessage id='guide.intro2.1' defaultMessage='default'/>
            </p> 
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='guide.intro2.2' defaultMessage='default'/>
            </p>             
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='guide.intro2.3' defaultMessage='default'/>
            </p>  

            <ExternalLinks
              imagePath={'/intro2.svg'}
              imageAlt={'quiz'}
              title1={'guide.intro2.link1'}
              description1={'guide.intro2.linkdescription1'}
              link1={'https://www.linkedin.com/pulse/why-do-we-play-games-david-mullich/'}
              title2={'guide.intro2.link2'}
              description2={'guide.intro2.linkdescription2'}
              link2={'https://www.nytimes.com/2020/06/11/style/why-people-love-games.html'}
              title3={'guide.intro2.link3'}
              description3={'guide.intro2.linkdescription3'}
              link3={'https://www.researchgate.net/publication/312623358_Why_do_people_play_games_A_meta-analysis'}
            />

          </div>
        </div>

        <QuoteHero
          imagePath={'/avatars/Aldo.jpg'}
          imageLeft={true}
          author={'guide.intro2.quote1author'}
          reference={'guide.intro2.quote1reference'}
          secondReference={'guide.intro2.quote1referenceb'}
          quote={'guide.intro2.quote1'}
        />

        <QuoteHero
          imagePath={'/avatars/Diana.png'}
          imageLeft={false}
          author={'guide.intro2.quote2author'}
          reference={'guide.intro2.quote2reference'}
          secondReference={'guide.intro2.quote2referenceb'}
          quote={'guide.intro2.quote2'}
        />

        <Lens
        textComponent={
          <> 
            <ul className='list-disc list-outside px-5 xl:px-18 pb-4'>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='guide.intro2.question1' defaultMessage='default'/>
              </li>           
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='guide.intro2.question2' defaultMessage='default'/>
              </li>  
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='guide.intro2.question3' defaultMessage='default'/>
              </li>  
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='guide.intro2.question4' defaultMessage='default'/>
              </li>  
            </ul>
          </>
        }/>

        <div className="container px-5 py-24 mx-auto">
          <div className="lg:w-3/4 w-full mx-auto text-left">

            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='guide.intro2.5' defaultMessage='default'/>
            </p>             
            <ul className='list-disc list-inside px-5 xl:px-18 pb-12'>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='guide.intro2.5a' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='guide.intro2.5b' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='guide.intro2.5c' defaultMessage='default'/>
              </li>
            </ul>  
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='guide.intro2.6' defaultMessage='default'/>
              <a href='#chapterTwo' className='shadow-link hover:shadow-linkThick'><FormattedMessage id='guide.intro2.link4' defaultMessage='default'/> </a>
              <FormattedMessage id='guide.intro2.6b' defaultMessage='default'/>

            </p>    
          </div>
        </div>
        <div className="w-full px-5 py-12 mx-auto bg-yellow">
          <div className="lg:w-3/4 w-full mx-auto text-center">
            <p className="leading-normal italic tracking-wide text-lg">
              <FormattedMessage id='guide.intro2.closing' defaultMessage='default'/>
              <span className="font-bold">
                <FormattedMessage id='guide.reading' defaultMessage='default'/>
              </span>
            </p> 
 
          </div>
        </div>
        
      </section>
    )
  }
}