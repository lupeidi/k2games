import React from "react";
import QuoteHero from '../quoteHero';
import ExternalLinks from '../externalLinks';

import { FormattedMessage } from "react-intl";

export default class introTwo extends React.Component {
  render() {

    return(
      <section className="text-gray-700 w-full body-font">
        <div className="container px-5 pt-24 pb-12 mx-auto">
          <div className="lg:w-3/4 w-full mx-auto text-left">

            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='guide.intro2.1' defaultMessage='default'/>
            </p> 
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='guide.intro2.2' defaultMessage='default'/>
            </p>             
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='guide.intro2.3' defaultMessage='default'/>
            </p>             


          </div>
        </div>
        
        <div className='flex flex-col sm:flex-row mx-auto xl:px-32 2xl:w-2/3'>
          <div className='w-full p-4 sm:w-1/2 xl:w-2/3 xl:pr-32 m-auto'>
            <img src='/intro2.svg' alt='quiz' className=''></img>
          </div>
          <div className='w-full p-4 sm:w-1/2 xl:w-1/3 m-auto'>
            <ExternalLinks/>
          </div>
        </div>

        <div className='flex flex-col xl:flex-row px-5'>
          <QuoteHero
            author={'guide.intro2.quote1author'}
            reference={'guide.intro2.quote1reference'}
            quote={'guide.intro2.quote1'}
          />

          <QuoteHero
            author={'guide.intro2.quote2author'}
            reference={'guide.intro2.quote2reference'}
            quote={'guide.intro2.quote2'}
          />
        </div>

      {/* lens on you */}
        <div className="container px-5 pt-24 pb-12 mx-auto">
          <div className="lg:w-3/4 w-full mx-auto text-left">

            <p className="pb-12 leading-normal font-bold text-xl">
              <FormattedMessage id='guide.intro2.4' defaultMessage='default'/>
            </p> 

            <ul className='list-disc list-inside px-5 xl:px-18 pb-12'>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='guide.intro2.question1' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='guide.intro2.question2' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='guide.intro2.question3' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='guide.intro2.question4' defaultMessage='default'/>
              </li>
            </ul>

            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='guide.intro2.5' defaultMessage='default'/>
            </p> 

            <ul className='list-disc list-inside px-5 xl:px-18 pb-4'>
              <li className='leading-normal tracking-wide text-lg pb-2'>
                <FormattedMessage id='guide.intro2.5a' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-2'>
                <FormattedMessage id='guide.intro2.5b' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-2'>
                <FormattedMessage id='guide.intro2.5c' defaultMessage='default'/>
              </li>
            </ul>

            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='guide.intro2.6' defaultMessage='default'/>
            </p> 

          </div>
        </div>
      </section>
    )
  }
}