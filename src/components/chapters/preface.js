import React from "react";
import { FormattedMessage } from "react-intl";

export default class Preface extends React.Component {
  render() {

    return(
      <section id='preface' className="text-dark w-full body-font">
        <div className="container px-5 py-12 mx-auto">
          <div className="lg:w-3/4 mx-auto text-left">

            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='preface.1' defaultMessage='default'/>
            </p> 
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='preface.2' defaultMessage='default'/>
            </p>             
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='preface.3' defaultMessage='default'/>
            </p>  
            <ul className='list-disc list-inside px-5 xl:px-18 pb-12'>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='preface.3.1' defaultMessage='default'/>
              </li>
              <li className='leading-normal tracking-wide text-lg pb-4'>
                <FormattedMessage id='preface.3.2' defaultMessage='default'/>
              </li>
            </ul>           
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='preface.4' defaultMessage='default'/>
            </p>             
            <p className="leading-normal tracking-wide text-lg pb-4">
              <FormattedMessage id='preface.5' defaultMessage='default'/>
            </p>             

          </div>
        </div>
      </section>
    )
  }
}