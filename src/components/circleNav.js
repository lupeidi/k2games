import React from "react";
import { FormattedMessage } from "react-intl";

export default class CircleNav extends React.Component {
  render() {
    return(
      <div className="min-w-screen bg-blue flex flex-col items-center p-5 lg:p-10 overflow-hidden relative">
        <div className="w-2/3 max-w-12xl rounded-lg my-6 py-12 bg-background shadow-xl p-5 md:px-12 mx-auto text-dark relative flex flex-col">

        <h2 className="pb-12 leading-normal font-bold text-center text-2xl">
          <FormattedMessage id='landing.nav' defaultMessage='default'/>
        </h2> 

        <div className=''>
          <div className='place-self-center'>
            <p className="leading-normal text-center tracking-wide text-lg">
              <a href='/guide' className='shadow-link hover:shadow-linkThick'><FormattedMessage id='link.1' defaultMessage='default'/></a>
            </p>
          </div>
        </div>


        {/* first row */}
        <div className='flex flex-row justify-between p-10'>
          {/* first arrow */}
          <div className='w-10 h-10 transform rotate-90 self-end m-auto'>
            <img src='/static/right-drawn-arrow.svg'></img>
          </div>
          {/* link */}

          {/* second arrow */}
          <div className='w-10 h-10 transform rotate-180 self-end m-auto'>
            <img src='/static/right-drawn-arrow.svg'></img>
          </div>
        </div>

        {/* second row */}
        <div className='flex flex-row justify-evenly py-5'>
          {/* first link */}
          <div className='xl:pr-24'>
            <p className="leading-normal tracking-wide text-right text-lg">
              <a href='/games' className='shadow-link hover:shadow-linkThick'><FormattedMessage id='link.2' defaultMessage='default'/></a>
            </p>
          </div>
          {/* second link */} 
          <div className='xl:pl-24'> 
            <p className="leading-normal tracking-wide text-left text-lg">
              <a href='/contact' className='shadow-link hover:shadow-linkThick'><FormattedMessage id='link.4' defaultMessage='default'/></a>
            </p>
          </div>
        </div>

        {/* third row */}
        <div className='flex flex-row justify-between p-10'>
          {/* first arrow */}
          <div className='w-10 h-10 self-end m-auto'>
            <img src='/static/right-drawn-arrow.svg'></img>
          </div>
          {/* link */}

          {/* second arrow */}
          <div className='w-10 h-10 transform -rotate-90 self-end m-auto'>
            <img src='/static/right-drawn-arrow.svg'></img>
          </div>
        </div>

        <div>
          <div className='place-self-center'>
            <p className="leading-normal text-center tracking-wide text-lg">
              <a target="_blank" href='https://www.facebook.com/groups/k2gamescluj' className='shadow-link hover:shadow-linkThick'><FormattedMessage id='link.3' defaultMessage='default'/></a>
            </p>
          </div>
        </div>

        </div>
      </div>
    )
  }
}