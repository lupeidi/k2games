import { FormattedMessage } from 'react-intl';
import React from "react";

export default class externalLinks extends React.Component {
  render() {
    const { imagePath, imageAlt, title1, description1, link1,
      title2, description2, link2, title3, description3, link3 } = this.props;

    return(
      <div className='flex flex-col sm:flex-row sm:pt-12 pt-0 '>
      <div className='sm:w-1/3 w-full m-auto pb-4 sm:pb-0'>
        <img src={imagePath} alt={imageAlt} className=''></img>
      </div>
        <div className='sm:w-2/3 w-full pl-0 sm:pl-12 m-auto'>
          <section className="text-dark body-font">
            <div className="container px-5 mx-auto">
              <div className="flex items-center mx-auto border-b pb-10 mb-10 border-gray-200 sm:flex-row flex-col">
    
                <div className="flex-grow sm:text-left text-center mt-6 sm:mt-0">
                  {title1 && 
                  <h2 className="text-gray-900 text-lg title-font font-medium mb-2"><FormattedMessage id={title1} defaultMessage='default'/></h2>
                  }
                  <p className="leading-relaxed text-base"><FormattedMessage id={description1} defaultMessage='default'/></p>
                  <p className="pt-6 text-base leading-6 font-bold sm:text-lg sm:leading-7">
                    <a target="_blank" href={link1} > <FormattedMessage id='guide.intro2.linkread' defaultMessage='default'/>&rarr; </a>
                  </p>
                </div>
              </div>

              <div className="flex items-center mx-auto border-b pb-10 mb-10 border-gray-200 sm:flex-row flex-col">
                <div className="flex-grow sm:text-left text-center mt-6 sm:mt-0">
                  {title2 &&
                  <h2 className="text-gray-900 text-lg title-font font-medium mb-2"><FormattedMessage id={title2} defaultMessage='default'/></h2>
                  }
                  <p className="leading-relaxed text-base"><FormattedMessage id={description2} defaultMessage='default'/></p>
                  <p className="pt-6 text-base leading-6 font-bold sm:text-lg sm:leading-7">
                    <a target="_blank" href={link2} > <FormattedMessage id='guide.intro2.linkread' defaultMessage='default'/>&rarr; </a>
                  </p>
                </div>
              </div>

              <div className="flex items-center mx-auto sm:flex-row flex-col">
                <div className="flex-grow sm:text-left text-center mt-6 sm:mt-0">
                  {title3 && 
                  <h2 className="text-gray-900 text-lg title-font font-medium mb-2"><FormattedMessage id={title3} defaultMessage='default'/></h2>
                  }
                  <p className="leading-relaxed text-base"><FormattedMessage id={description3} defaultMessage='default'/></p>
                  <p className="pt-6 text-base leading-6 font-bold sm:text-lg sm:leading-7">
                    <a target="_blank" href={link3} > <FormattedMessage id='guide.intro2.linkread' defaultMessage='default'/>&rarr; </a>
                  </p>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    )
  }
};
        