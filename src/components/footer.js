import { FormattedMessage } from 'react-intl';
import React from "react";

export default class Footer extends React.Component {
  render() {
    return(
      <section className="flex flex-col md:flex-row w-full border-t p-6 md:p-12 justify-evenly">
        {/* erasmus logo & text */}
        <div className='md:w-2/3 px-2'>
          <img src='/erasmusLogo.svg' className='justify-start max-h-12'/>
          <div className='pt-5 md:pt-0 leading-snug text-xs'>
            <FormattedMessage id={'license'} defaultMessage='default'/>
          </div>
        </div>

        {/* creative commons logo & text */}
        <div className='md:w-1/3 px-2 pt-5 md:pt-0 '>
          <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">
            <img alt="Creative Commons License" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" className='justify-start max-h-12'/>
          </a>
          <br />
          <div className='leading-snug text-xs'>
            <FormattedMessage id={'creativeCommons'} defaultMessage='default'/>
            <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">
              <FormattedMessage id={'creativeCommonsName'} defaultMessage='default'/>
            </a>
          </div>
          
        </div>
      </section>
    )
  }
}