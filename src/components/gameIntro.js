import React from "react";
import { FormattedMessage } from "react-intl";

export default class NgoCard extends React.Component {
  render() {
    const { title, description, descriptionB, image, imageAlt, buttonColor, downloadPath, downloadAlt, preview} = this.props;

    return(
        <div className="py-8 flex flex-col items-center">

          <div className="flex md:flex-row flex-col">
            <div className="w-full md:w-2/3 p-1 sm:p-8">

              <h1 className="flex-1 leading-relaxed text-left text-4xl font-bold pb-6 px-6">
                <FormattedMessage id={title} defaultMessage='default'/>
              </h1>

              <p className='flex-1 leading-normal text-left text-base font-normal pb-6 px-6'>
                <FormattedMessage id={description} defaultMessage='default'/>
              </p>

              <p className='flex-1 leading-normal text-left text-base font-normal pb-6 px-6'>
                <FormattedMessage id={descriptionB} defaultMessage='default'/>
              </p>

              <p className='flex-1 leading-normal text-left text-base font-normal pb-6 px-6'>
                <FormattedMessage id={'games.printandplay'} defaultMessage='default'/>
              </p>

            </div>

            {/* <div className='w-0 md:w-1/3 p-0 md:p-8 md:pb-6 md:pt:8'> */}
            <div className='m-auto pb-12 md:pb-0'>
              <img src={image} alt={imageAlt}></img>
            </div>
          </div>

          {!preview &&
          <div className="">
            <button type="submit" className=''>
              <a href={downloadPath} download={downloadAlt} className={"inline-block px-8 py-2 text-white rounded-lg font-bold leading-relaxed tracking-wider " + buttonColor}>
                <FormattedMessage id={"download"} defaultMessage='default'/>
              </a>
            </button>
          </div>
          }
          {preview &&
          <div className="">
            <button type="submit" className=''>
              <a href={downloadPath} target="_blank" className={"inline-block px-8 py-2 text-white rounded-lg font-bold leading-relaxed tracking-wider " + buttonColor}>
                <FormattedMessage id={"openFile"} defaultMessage='default'/>
              </a>
            </button>
          </div>
          }
        </div>
    )
  }
}