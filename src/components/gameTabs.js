import React from "react";

export default class GameTabs extends React.Component {
  render() {
    return (
      <>
        <div className="flex flex-wrap">
          <div className="w-full">

            {/* Tabs */}
            <ul role="tablist" className='flex flex-wrap flex-row justify-around my-4 sm:mb-4 md:mb-12 xl:mb-24'>
              <li className='w-full sm:w-1/2 md:w-1/3 xl:w-1/6'>
              <a href="/games/pioneer-city">
                  <img src="/gameLogos/K2 Pioneer City logo - full color.png" alt="Pioneer City" className=""/>
                </a>
              </li>

              <li className='w-full sm:w-1/2 md:w-1/3 xl:w-1/6'>
                <a href="/games/recycling-party">
                  <img src="/gameLogos/K2 Recycling Party logo - full color.png" alt="K2 Recycling Party" className=" "/>
                </a>
              </li>

              <li className='w-full sm:w-1/2 md:w-1/3 xl:w-1/6'>
                <a href="/games/air-quality">
                  <img src="/gameLogos/K2 Air Quality logo - full color.png" alt="K2 Air Quality" className=" "/>
                </a>
              </li>

              <li className='w-full sm:w-1/2 md:w-1/3 xl:w-1/6'>
              <a href="/games/waste-management">
                  <img src="/gameLogos/K2 Waste Management logo - full color.png" alt="K2 Waste Management" className=" "/>
                </a>
              </li>

              <li className='w-full sm:w-1/2 md:w-1/3 xl:w-1/6'>
                <a href="/games/climate-negociations">
                  <img src="/gameLogos/K2 Climate Negociations logo - full color.png" alt="K2 Climate Negociations" className=" "/>
                </a>
              </li>
              <li className='w-full sm:w-1/2 md:w-1/3 xl:w-1/6'>
                <a href="/games/city-gardens">
                  <img src="/gameLogos/K2 City Gardens logo - full color.png" alt="K2 City Gardens" className=" "/>
                </a>
              </li>
            </ul>

          </div>
        </div>
      </>
    );
  }
};
// K2 Climate Negociations logo - white background