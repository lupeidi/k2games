import { FormattedMessage } from 'react-intl';
import React from "react";
import LanguageSelector from './languageSelector';

export default class Header extends React.Component {
  render() {
    return(
      <section className="flex flex-row fixed z-50 box-border justify-evenly w-full h-16 xl:h-20 shadow-lg bg-yellow">

        <div className='self-center flex-1 px-2 sm:px-5 lg:px-10 2xl:p-14'>
          <a href='/' className="content-start" >
            <img src="/k2games.svg" alt="K2Games Logo" className="max-h-10 md:max-w-lg max-w-xs opacity-0.7"/>
          </a>
        </div>

        <div className='self-center text-center px-1 sm:px-5 lg:px-10 2xl:p-14 text-dark font-sans font-bold	tracking-wide	sm:text-md md:text-xl lg:text-2xl transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-125'>
          <a href='/guide' >
            <FormattedMessage id='header.guide' defaultMessage='default'/>
          </a>
        </div>
        <div className='self-center text-center px-1 sm:px-5 lg:px-10 2xl:p-14 text-dark font-sans font-bold	tracking-wide	sm:text-md md:text-xl lg:text-2xl transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-125'>
          <a href='/games' >
            <FormattedMessage id='header.games' defaultMessage='default'/>
          </a>
        </div>


        <div className='self-center text-center px-1 sm:px-5 lg:px-10 2xl:p-14 text-dark font-sans font-bold	tracking-wide sm:text-md md:text-xl lg:text-2xl transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-125 '>
          <a href='/aboutus' >
            <FormattedMessage id='header.contact' defaultMessage='default'/>
          </a>
        </div>

        {/* <div className='self-center px-1 sm:px-5 lg:px-10 2xl:p-14'>
          <LanguageSelector/>
        </div> */}

      </section>
    )
  }
}
