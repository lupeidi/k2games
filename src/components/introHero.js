import React from "react";
import { FormattedMessage } from "react-intl";

export default class introHero extends React.Component {
  render() {

    return(
      <section className="text-dark w-full body-font flex flex-col lg:flex-row px-1 sm:px-12 pt-24 sm:pt-44">
        {/* text div */}
        <div className='w-full lg:w-3/5 px-5 mx-auto'>
          <h1 className="tracking-wide font-bold text-left text-5xl lg:text-7xl sm:text-8xl pb-16 ">
            <FormattedMessage id='landing.title' defaultMessage='default'/>
          </h1> 
          <p className="leading-normal tracking-wide text-xl pb-4">
            <FormattedMessage id='landing.1' defaultMessage='default'/>
          </p>  
          <p className="leading-normal tracking-wide text-xl pb-4">
            <FormattedMessage id='landing.2' defaultMessage='default'/>
          </p>             
          <ul className='list-disc list-inside px-5 xl:px-18'>
            <li className='leading-normal tracking-wide text-xl pb-4'>
              <FormattedMessage id='landing.3.1' defaultMessage='default'/>
            </li>
            <li className='leading-normal tracking-wide text-xl pb-4'>
              <FormattedMessage id='landing.3.2' defaultMessage='default'/>
            </li>
            <li className='leading-normal tracking-wide text-xl pb-4'>
              <FormattedMessage id='landing.3.3' defaultMessage='default'/>
            </li>
          </ul>           
          <p className="leading-normal tracking-wide text-xl pb-4">
            <FormattedMessage id='landing.4' defaultMessage='default'/>
          </p>
        </div>
        {/* image div */}
        <div className='w-full lg:w-2/5 px-5 mx-auto'>
          <img src='/hello.svg' alt="road ahead" className='m-auto'/>
        </div>

      </section>
    )
  }
}