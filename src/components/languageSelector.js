import Link from 'next/link';
import { withRouter } from 'next/router';
import React from "react";

class LanguageSelector extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showOptions: false, 
      languages: [{
        code: "en",
        name: "English",
      },
      {
        code: "ro",
        name: "Română",
      }]
    }
  }

  toggleOptions() {
    this.setState({showOptions: !this.state.showOptions})
  }

  render() {
    const { showOptions, languages } = this.state;
    const page = this.props.router.pathname.split('/').slice(2).join('/');

    return (
      <>
        <button className='pt-2 sm:py-8' onClick={() => this.toggleOptions()}>
          <img className='h-6 sm:h-8 xl:h-10 object-contain' src='/static/languageSwitcher.svg' />
        </button>

        {showOptions && 
        <div className='fixed z-0 inset-0 h-full w-full cursor-default' onClick={() => this.toggleOptions()} />
        }

        {showOptions &&
          <div class="origin-top-right absolute mt-2 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5">
            <div class="py-1" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
              {languages.map((language) => 
                <div onClick={() => this.toggleOptions()}>
                  <Link as={`/${language.code}/${page}`} href={'/[[lang]]'}>
                    <a className='text-gray-800 px-4 cursor-pointer'>{language.name}</a>
                  </Link>
                </div>
              )}
            </div>
          </div>
        }
      </>
    );
  }
}

export default withRouter(LanguageSelector)
