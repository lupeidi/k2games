import React from "react";
import { FormattedMessage } from "react-intl";

export default class lens extends React.Component {
  render() {
    const { textComponent } = this.props;

    return(
      <div className=" bg-blue flex items-center p-1 sm:p-5 lg:p-10 overflow-hidden relative">
        <div className="w-full max-w-10xl p-1 sm:p-5 lg:p-20 mx-auto text-dark relative md:text-left">
          
          <div className="float-left w-full my-auto">

            <h1 className="pb-12 md:pb-16 md:px-16 px-6 leading-normal font-bold text-4xl">
              <FormattedMessage id='guide.lens' defaultMessage='default'/>
            </h1> 

            {/* image */}
            <div className="float-right">
              <div className="relative">
                <img src="/lens.svg" className="md:max-w-md xl:max-w-xl z-10 p-6 md:p-0 md:pl-6" alt="looking for answers"/>
              </div>
            </div>

            {/* text */}
            <div className="md:px-16 px-1 sm:px-6 my-auto">
              <div className="my-auto">
                {textComponent}
              </div>
            </div>

          </div>
          
        </div>
      </div>
    )
  }
}
