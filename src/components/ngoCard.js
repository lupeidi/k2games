import React from "react";
import { FormattedMessage } from "react-intl";

export default class NgoCard extends React.Component {
  render() {
    const { logoImgPath, contactLines, intro, websiteLink, title } = this.props;

    return(
      <div className="py-4 px-4 flex flex-col justify-center">
        <div className="relative py-3 sm:max-w-md sm:mx-auto">
          <div className="absolute inset-0 bg-blue opacity-50 shadow-xl transform -skew-y-6 sm:skew-y-0 sm:-rotate-6 sm:rounded-3xl"></div>
          <div className="relative py-10 bg-white shadow-lg sm:rounded-3xl sm:p-20">
            <div className="max-w-md mx-auto">
              <div>
                <img src={logoImgPath} className="max-w-40 mx-auto" />
              </div>
              <div className="divide-y divide-gray-200">


                <div className="py-8 text-base leading-6 space-y-4 text-dark sm:text-lg sm:leading-7">
                  <div className='pb-4 font-bold pt-4 text-2xl'>
                    <FormattedMessage id={title} defaultMessage='default'/>
                  </div>
                  <div className='pb-4'>
                    <FormattedMessage id={intro} defaultMessage='default'/>
                  </div>
                  {contactLines?.map((contactLine)=>
                  <div>
                    <FormattedMessage id={contactLine} defaultMessage='default'/>
                  </div>)}
                </div>
                {websiteLink &&
                <div className="pt-6 text-base leading-6 font-bold sm:text-lg sm:leading-7">
                  <FormattedMessage id='ngoCard.question' defaultMessage='default'/>
                  <p className="text-cyan-600 hover:text-cyan-700">
                    <a href={websiteLink} > <FormattedMessage id='ngoCard.visit' defaultMessage='default'/>&rarr; </a>
                  </p>
                </div>
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}