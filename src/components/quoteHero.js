import React from "react";
import { FormattedMessage } from "react-intl";

export default class QuoteHero extends React.Component {
  render() {
    const { author, quote, reference, secondReference, imagePath, imageLeft } = this.props;

    return(
      <section className="text-dark body-font border-t border-b  border-gray-200">


        <div className="container py-24 m-auto flex flex-col md:flex-row">
          { imageLeft && 
            <img src={imagePath} className='hidden md:block max-h-xl object-cover w-full md:w-1/3 my-auto md:mr-4 rounded-xl pb-8 md:pb-0 px-2 md:px-0'/>
          }
          <img src={imagePath} className='block md:hidden max-h-xl object-cover w-full m-auto rounded-xl mb-8 px-2'/>
          <div className="lg:w-3/4 w-full md:w-2/3 m-auto text-center">
    
            <h2 className="text-dark font-bold title-font tracking-wider text-xl">              
              <FormattedMessage id={author} defaultMessage='default'/>
            </h2>
            { reference &&             
              <p className="text-gray-500">
                <FormattedMessage id={reference} defaultMessage='default'/>
              </p>
            }
            { secondReference &&
              <p className="text-gray-500">
                <FormattedMessage id={secondReference} defaultMessage='default'/>
              </p>
            }

            <span className="inline-block h-1 w-10 rounded bg-orange mt-8 mb-6"></span>

            <div className="leading-relaxed text-lg p-1">
              <FormattedMessage id={quote} defaultMessage='default'/>
            </div>

            <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" className="inline-block w-8 h-8 text-orange mt-8" viewBox="0 0 975.036 975.036">
              <path d="M925.036 57.197h-304c-27.6 0-50 22.4-50 50v304c0 27.601 22.4 50 50 50h145.5c-1.9 79.601-20.4 143.3-55.4 191.2-27.6 37.8-69.399 69.1-125.3 93.8-25.7 11.3-36.8 41.7-24.8 67.101l36 76c11.6 24.399 40.3 35.1 65.1 24.399 66.2-28.6 122.101-64.8 167.7-108.8 55.601-53.7 93.7-114.3 114.3-181.9 20.601-67.6 30.9-159.8 30.9-276.8v-239c0-27.599-22.401-50-50-50zM106.036 913.497c65.4-28.5 121-64.699 166.9-108.6 56.1-53.7 94.4-114.1 115-181.2 20.6-67.1 30.899-159.6 30.899-277.5v-239c0-27.6-22.399-50-50-50h-304c-27.6 0-50 22.4-50 50v304c0 27.601 22.4 50 50 50h145.5c-1.9 79.601-20.4 143.3-55.4 191.2-27.6 37.8-69.4 69.1-125.3 93.8-25.7 11.3-36.8 41.7-24.8 67.101l35.9 75.8c11.601 24.399 40.501 35.2 65.301 24.399z"></path>
            </svg>
          </div>

          { !imageLeft && 
            <img src={imagePath} className='hidden md:block max-h-xl object-cover my-auto w-full md:w-1/3 md:ml-4 rounded-xl pt-8 md:pt-0 px-2 md:px-0'/>
          }
        </div>
      </section>
    )
  }
}