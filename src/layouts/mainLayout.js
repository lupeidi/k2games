import Header from './../components/header';
import Footer from './../components/footer';
import React from 'react';

export default function MainLayout(props){
  var { content } = props;

  return(
    <>
      <title>K2Games</title>
      <Header/>
      <div className='w-full'/>
      <div className='bg-background'>
        {content}
      </div>
      <Footer/>
    </>
  )
}