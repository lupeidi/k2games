module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './src/components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      maxHeight: {
        "2xs": "16rem",
        xs: "20rem",
        sm: "24rem",
        md: "28rem",
        lg: "32rem",
        xl: "36rem",
        "2xl": "42rem",
        "3xl": "48rem",
        "4xl": "56rem",
        "5xl": "64rem",
        "6xl": "72rem",
      },
      colors: {
        'pioneer-city': '#F7941D',
        'recycling-party': '#8DC63F',
        'air-quality': '#00AEEF',
        'waste-management': '#FFCB0B',
        'climate-negociations': '#5E2E86',
        'city-gardens': '#00863D',
        'background': '#FEFFFF',
        'backgroundDarker': '#fefefe',
        'dark': '#3f3d56',
        'yellow': '#ffe167',
        'green': '#ABD572',
        'blue': '#C2EFFF',
        'orange': '#F7941D',          
      },
      boxShadow: {
        link: '0 -4px 0 0 rgba(247, 148, 29, .7) inset',
        linkThick: '0 -12px 0 0 rgba(247, 148, 29, .7) inset',

      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
