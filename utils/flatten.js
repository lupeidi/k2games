export default function flatten(obj, prefix = false, result = null) {
  result = result || {};

  //Preserve empty arrays, they are lost otherwise
  if(Array.isArray(obj) && obj.length < 1) {
    result[prefix] = [];
  }

  prefix = prefix? prefix + '.' : '';

  for ( const i in obj ) {
    if(Object.prototype.hasOwnProperty.call(obj, i)) {
      if(typeof obj[i] === 'object' && obj[i] !== null) {
        //Recursion on deeper objects
        flatten(obj[i], prefix + i, result);
      } else {
        result[prefix] = obj[i];
      }
    }
  }

  return result;
}